// Load required packages
var Event = require('../models/event');

// Create endpoint /api/events for POST
exports.postEvents = function(req, res) {
  // Create a new instance of the Event model
  var event = new Event();

  // Set the event properties that came from the POST data
  event.name = req.body.name;
  event.eventType = req.body.eventType;
  event.eventParams = req.body.eventParams;
  event.description = req.body.description;

  // Save the event and check for errors
  event.save(function(err) {
    if (err)
      res.send(err);

    res.json({ message: 'APICALL: Event added to the planstr!', data: event });
  });
};

// Create endpoint /api/events for GET
exports.getEvents = function(req, res) {
  // Use the Event model to find all event
  Event.find({ userId: req.user._id }, function(err, events) {
    if (err)
      res.send(err);

    res.json(events);
  });
};

// Create endpoint /api/events/:event_id for GET
exports.getEvent = function(req, res) {
  // Use the Event model to find a specific event
  Event.find({ userId: req.user._id, _id: req.params.event_id }, function(err, event) {
    if (err)
      res.send(err);

    res.json(event);
  });
};

// Create endpoint /api/events/:event_id for PUT
exports.putEvent = function(req, res) {
  // Use the Event model to find a specific event
  Event.update({ userId: req.user._id, _id: req.params.event_id }, { quantity: req.body.quantity }, function(err, num, raw) {
    if (err)
      res.send(err);

    res.json({ message: 'APICALL: '+num + ' updated' });
  });
};

// Create endpoint /api/events/:event_id for DELETE
exports.deleteEvent = function(req, res) {
  // Use the Event model to find a specific event and remove it
  Event.remove({ userId: req.user._id, _id: req.params.event_id }, function(err) {
    if (err)
      res.send(err);

    res.json({ message: 'APICALL: Event removed from the planstr!' });
  });
};