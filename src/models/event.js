// Load required packages
var mongoose = require('mongoose');

// Define our event schema
var EventSchema   = new mongoose.Schema({
  name: String,
  eventType: String,
  eventParams: String,
  description: String
});

// Export the Mongoose model
module.exports = mongoose.model('Event', EventSchema);