// server.js

// set up ======================================================================
// get all the tools we need
var fortune  = require('./lib/fortune.js');
var express  = fortune.express;
var app      = express();
var port     = process.env.PORT || 8080;
var mongoose = require('mongoose');
var passport = require('passport');
var flash    = require('connect-flash');
var paginate = require('express-paginate')
var mongoPage= require('mongoose-paginate');

var morgan       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');

var configDB = require('./config/database.js');
// API CALL ====================================================================
var eventController = require('./controllers/event');
var userController = require('./controllers/user');
var authController = require('./controllers/auth');

//app.use( express.static( "public" ) );
// configuration ===============================================================
mongoose.connect(configDB.url); // connect to our database

require('./config/passport')(passport); // pass passport for configuration

// set up our express application
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.json()); // get information from html forms
app.use(bodyParser.urlencoded({ extended: true }));

app.set('view engine', 'ejs'); // set up ejs for temp/lating

// required for passport
app.use(session({ secret: 'planstr@pLaNsTrDev+TeAm123' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

// api call routing ======================================================================
// Create our Express router
var router = express.Router();

// Create endpoint handlers for /events
//router.route('/events')
//  .post(authController.isAuthenticated, eventController.postEvents)
//  .get(authController.isAuthenticated, eventController.getEvents);

// Create endpoint handlers for /events/:event_id
//router.route('/events/:event_id')
//  .get(authController.isAuthenticated, eventController.getEvent)
//  .put(authController.isAuthenticated, eventController.putEvent)
//  .delete(authController.isAuthenticated, eventController.deleteEvent);

// Create endpoint handlers for /users
//router.route('/users')
//  .post(userController.postUsers)
//  .get(authController.isAuthenticated, userController.getUsers);

// Register all our routes with /api
app.post('/ReceiveJSON', function(req, res){
  var r= JSON.parse(JSON.stringify(req.body, null, 2));
  console.log(JSON.stringify(req.body, null, 2));
  console.log("VAL:"+ (parseInt(r.user)+parseInt(r.data)).toString());

  res.send("ok");
});

var jsonapi = fortune({
        adapter: 'mongodb',
        //namespace:'/api/v1',
        //connectionString
//        username:'nodejitsu',
//        password:'b6b579d00b8f33f17b970c8f9ffbd959',
//        host:'troup.mongohq.com',
//        port:'10042',
        db: 'nodejitsudb1704241274'


    });


jsonapi.resource('user', {
            _id: String,
            firstname: String,
            lastname: String,
            fullname: String,
            email: String,
            password: String,
            //participations:['participants'],
            salt: String
        });
jsonapi.after('user', function () {
    this.href='/users/'+this.id;
    //delete this.salt;
    return this;
});
//jsonapi.adapter.schema.plugin(mongoPage);
jsonapi.resource('participant', {
            _id: String,
            user: 'user',
            userGroupId: Array,
            event: 'event'
        });

jsonapi.after('participant', function () {
    this.href='/participants/'+this.id;
/*    if (!!this.links)
    {
        //console.log('Find Link');
        if (!!this.links.user)
        {
            //console.log('Find User '+'/user/'+this.links.user);
            var uid=this.links.user;
            //
            this.links.user= JSON.parse('{"id":"'+uid+'","href":"/users/'+uid+'","type":"user"}');

        }
        if (!!this.links.event)
        {
            console.log('Find Event');
            var uid=this.links.event;
            this.links.event= JSON.parse('{"id":"'+uid+'","href":"/events/'+uid+'","type":"event"}');
        }
    }*/
    console.log(JSON.stringify(this,null,2));
    console.log('------------------------------------------------');
   return this;
});

jsonapi.resource('event', {
            _id: String,
            name: String,
            description: String,
            eventTypeId: Array,
            //participators:['participants'],
            eventParams: String
        });
jsonapi.after('event', function () {
    this.href='/events/'+this.id;
    return this;
});
//jsonapi.use(paginate.middleware(10,50));
jsonapi.listen(1337);
app.use('/api', router);


// routes ======================================================================
require('./app/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport

// launch ======================================================================
app.listen(port);
console.log('The planstr starts on port ' + port);
console.log('FortuneTeller: ' + JSON.stringify(jsonapi.options,null,2));
